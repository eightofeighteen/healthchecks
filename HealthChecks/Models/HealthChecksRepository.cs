﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HealthChecks.Models
{
    public class HealthChecksRepository : IHealthChecksRepository
    {
        private HealthChecksEntities _dataContext;

        public HealthChecksRepository()
        {
            _dataContext = new HealthChecksEntities();
        }

        public IList<Site> GetAllSites()
        {
            var query =
                from a in _dataContext.Sites
                select a;
            return query.ToList();
        }

        public IList<CheckDef> GetAllCheckDefsBySiteID(int siteId)
        {
            var query =
                from a in _dataContext.CheckDefs
                where (a.SiteID == siteId)
                orderby a.OrderNum
                select a;
            return query.ToList();
        }
    }
}