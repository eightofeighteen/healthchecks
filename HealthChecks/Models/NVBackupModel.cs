﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;

namespace HealthChecks.Models
{
    public class NVBackupModel
    {
        public NVBackupModel()
        {
            AvailableHosts = new List<SelectListItem>();
            AvailableHosts = new List<SelectListItem>();
            AvailableSites = new List<SelectListItem>();
            AvailableCheckDefs = new List<SelectListItem>();
            AvailableAutoTypes = new List<SelectListItem>();
        }
        [Display(Name = "Hosts")]
        public string host { get; set; }
        public IList<SelectListItem> AvailableHosts { get; set; }
        [Display(Name = "Titles")]
        public string TitleId { get; set; }
        public IList<SelectListItem> AvailableTitles { get; set; }

        [Display(Name = "Site")]
        public int SiteId { get; set; }
        public IList<SelectListItem> AvailableSites { get; set; }
        [Display(Name = "CheckDefs")]
        public int CheckDefId { get; set; }
        public IList<SelectListItem> AvailableCheckDefs { get; set; }

        public IList<SelectListItem> AvailableAutoTypes { get; set; }
    }
}