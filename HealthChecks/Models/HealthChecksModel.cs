﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;

namespace HealthChecks.Models
{
    public class HealthChecksModel
    {
        public HealthChecksModel()
        {
            AvailableSites = new List<SelectListItem>();
            AvailableCheckDefs = new List<SelectListItem>();
        }
        [Display(Name = "Site")]
        public int SiteId { get; set; }
        public IList<SelectListItem> AvailableSites { get; set; }
        [Display(Name = "CheckDefs")]
        public int CheckDefId { get; set; }
        public IList<SelectListItem> AvailableCheckDefs { get; set; }
    }
}