﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HealthChecks.Models
{
    public static class Flags
    {
        public static int FLAG_SITEADMIN = 32768;

        public static bool ContainsBit(int bits, int bit)
        {
            if ((bits & bit) == bit)
                return true;
            return false;
        }

        public static bool ContainsBit(UInt32 bits, UInt32 bit)
        {
            if ((bits & bit) == bit)
                return true;
            return false;
        }
    }
}