﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthChecks.Models
{
    public interface IHealthChecksRepository
    {
        IList<Site> GetAllSites();
        IList<CheckDef> GetAllCheckDefsBySiteID(int siteId);
    }
}
