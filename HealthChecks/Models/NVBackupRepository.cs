﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using HealthChecks.Models;

namespace HealthChecks.Models
{
    public class NVBackupRepository : INVBackupRepository
    {
        private NvBackupInfoDBDataContext _dataContext;
        private HealthChecksEntities _hcDataContext;

        public NVBackupRepository()
        {
            _dataContext = new NvBackupInfoDBDataContext();
            _hcDataContext = new HealthChecksEntities();

        }

        public IList<v_DistinctClient> GetAllHosts()
        {
            var query =
                from a in _dataContext.v_DistinctClients
                select a;
            return query.ToList<v_DistinctClient>();
        }

        private Boolean ListContainsBackupJob(List<job> l, job j)
        {
            foreach (job elem in l)
            {
                if (elem.Client == j.Client && elem.ID == j.ID && elem.Title == j.Title)
                    return true;
            }
            return false;
        }

        private List<job> StripInfo(List<job> list)
        {
            List<job> newList = new List<job>();
            foreach (job j in list)
            {
                j.Log = "";
                newList.Add(j);
            }
            return newList;
        }

        public IList<job> GetAllTitlesForHost(string host)
        {
            var query =
                from a in _dataContext.jobs
                where (a.Timestamp >= DateTime.Now.AddDays(-7) && a.Client == host)
                select a;
            List<job> l = new List<job>();
            foreach (job j in query)
            {
                if (!ListContainsBackupJob(l, j))
                    l.Add(j);
            }
            return StripInfo(l);
        }

        public IList<Site> GetAllSites()
        {
            var query =
                from a in _hcDataContext.Sites
                select a;
            return query.ToList();
        }

        public IList<CheckDefMin> GetAllCheckDefsBySiteID(int siteId)
        {
            var query =
                from a in _hcDataContext.CheckDefs
                where (a.SiteID == siteId)
                orderby a.OrderNum
                select a;
            IList<CheckDefMin> list = new List<CheckDefMin>();
            foreach (CheckDef elem in query)
            {
                CheckDefMin s = new CheckDefMin(elem.ID, elem.SiteID, elem.Name, elem.OrderNum);
                list.Add(s);
            }
            return list;
        }

        public IList<AutoType> GetAllAutoTypes()
        {
            var query =
                from a in _hcDataContext.AutoTypes
                select a;
            return query.ToList();
        }

        public string GetJobIDFromClientAndTitle(string client, string title)
        {
            var query =
                from a in _dataContext.jobs
                where (a.Client == client && a.Title == title)
                select a;
            if (query.Count() > 0)
                return query.ToList()[0].ID;
            return "0";
        }

        public void AddJobCheckMaps(List<JobCheckMap> map)
        {
            foreach (JobCheckMap elem in map)
                _hcDataContext.JobCheckMaps.Add(elem);
            _hcDataContext.SaveChanges();
        }
    }
}