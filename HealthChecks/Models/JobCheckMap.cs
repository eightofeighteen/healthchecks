//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace HealthChecks.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class JobCheckMap
    {
        public int ID { get; set; }
        public int CheckDefID { get; set; }
        public string JobID { get; set; }
        public string JobClient { get; set; }
        public int AutoTypeID { get; set; }
    
        public virtual AutoType AutoType { get; set; }
        public virtual CheckDef CheckDef { get; set; }
    }
}
