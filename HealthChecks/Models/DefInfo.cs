﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HealthChecks.Models
{
    public class DefInfo
    {
        public string fullName { get; set; }
        public int orderNum { get; set; }
        public string shortName { get; set; }
        public string parentName { get; set; }
        public int parentID { get; set; }
        public int level { get; set; }
    }
}