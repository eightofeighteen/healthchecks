﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HealthChecks.Models
{
    public class SiteInfo
    {
        public string sitename { get; set; }
        public int siteid { get; set; }
        public List<User> users { get; set; }
        public Dictionary<string, int> userFlags { get; set; }
        public List<CheckDef> defs { get; set; }
        public string defstring { get; set; }
        public List<Group> groups { get; set; }
        public Dictionary<string, int> groupFlags { get; set; }

        public bool AdminUser(string username)
        {
            if (userFlags != null && userFlags.Keys.Contains(username))
            {
                int flags = userFlags[username];
                return Flags.ContainsBit(flags, Flags.FLAG_SITEADMIN);
            }
            return false;
        }
    }
}