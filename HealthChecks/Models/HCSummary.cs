﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HealthChecks.Models
{
    public struct Update
    {
        public DateTime first { get; set; }
        public DateTime last { get; set; }
    }

    public class HCSummary
    {
        public int id { get; set; }
        public string siteName { get; set; }
        public DateTime firstUpdate { get; set; }
        public DateTime lastUpdate { get; set; }
        public Dictionary<DateTime, List<Update>> updateHistory { get; set; }
    }

    public class HCSummaryMatrix
    {
        public int id { get; set; }
        public string siteName { get; set; }
        public Dictionary<int, Update> updateHistory { get; set; }
    }
}