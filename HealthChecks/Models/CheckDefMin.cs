﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HealthChecks.Models
{
    public class CheckDefMin
    {
        public CheckDefMin(int ID, int siteID, string name, int orderNum)
        {
            this.ID = ID;
            this.siteID = siteID;
            this.name = name;
            this.orderNum = orderNum;
        }

        public int ID { get; set; }
        public int siteID { get; set; }
        public string name { get; set; }
        public int orderNum { get; set; }
    }
}