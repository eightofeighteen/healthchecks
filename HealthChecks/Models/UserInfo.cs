﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HealthChecks.Models
{
    public class UserInfo
    {
        public string username { get; set; }
        public string active { get; set; }
        public string admin { get; set; }

        public bool IsActive()
        {
            return (active == "on");
        }

        public bool IsAdmin()
        {
            return (admin == "on");
        }

        public UserInfo()
        {
            active = "";
            admin = "";
        }

        public int Flags()
        {
            int flag = 0;
            if (IsAdmin())
                flag += 32768;
            return flag;
        }
    }
}