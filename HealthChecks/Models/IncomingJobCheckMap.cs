﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;

namespace HealthChecks.Models
{
    public class IncomingJobCheckMap
    {
        public IncomingJobCheckMap()
        {
            
        }

        public IncomingJobCheckMap(string json)
        {
            IncomingJobCheckMap m = Newtonsoft.Json.JsonConvert.DeserializeObject<IncomingJobCheckMap>(json);
            siteID = m.siteID;
            checkID = m.checkID;
            backupHost = m.backupHost;
            backupJob = m.backupJob;
            type = m.type;
            fSiteID = m.fSiteID;
            fCheckID = m.fCheckID;
            fType = m.fType;
        }

        public int siteID { get; set; }
        public int checkID { get; set; }
        public string backupHost { get; set; }
        public string backupJob { get; set; }
        public int type { get; set; }
        public string fSiteID { get; set; }
        public string fCheckID { get; set; }
        public string fType { get; set; }
    }
}