﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HealthChecks.Models
{
    public class HCReport
    {
        public string sitename { get; set; }
        public int siteid { get; set; }
        public DateTime startdate { get; set; }
        public DateTime enddate { get; set; }
        public List<CheckDef> defList { get; set; }
        public List<CheckElement> elemList { get; set; }
        public Dictionary<int, string> results { get; set; }
        public Dictionary<int, CheckDef> defMap { get; set; }
        public string comments { get; set; }
        //public List<Dictionary<DateTime, string>> elemDTMap { get; set; }
        public Dictionary<int, Dictionary<DateTime, string>> elemDTMap { get; set; }
        public Dictionary<int, Dictionary<DateTime, string>> elemUserMap { get; set; }
        public Dictionary<int, Dictionary<DateTime, string>> elemCommentMap { get; set; }
        public Dictionary<DateTime, DateTime> lastUpdate { get; set; }
        public List<string> usernames { get; set; }
    }
}