﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthChecks.Models
{
    public interface INVBackupRepository
    {
        IList<v_DistinctClient> GetAllHosts();
        IList<job> GetAllTitlesForHost(string host);

        IList<Site> GetAllSites();
        IList<CheckDefMin> GetAllCheckDefsBySiteID(int siteId);

        IList<AutoType> GetAllAutoTypes();
        string GetJobIDFromClientAndTitle(string client, string title);
        void AddJobCheckMaps(List<JobCheckMap> map);
    }
}
