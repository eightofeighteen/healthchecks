﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using HealthChecks.Models;

namespace HealthChecks.Controllers
{
    public class HealthChecksController : Controller
    {
        private HealthChecksEntities db = new HealthChecksEntities();

        private int UserID(string username)
        {
            var userQuery =
                from a in db.Users
                where (a.Username.ToLower() == username.ToLower())
                select a;

            int userID = 0;
            foreach (var elem in userQuery)
            {
                userID = elem.ID;
            }
            return userID;
        }

        private string SiteName(int id)
        {
            var siteQuery =
                from a in db.Sites
                where (a.ID == id)
                select a;
            foreach (var elem in siteQuery)
                return elem.Name;
            return "";
        }

        private bool UserAuth(int id)
        {
            /*string username = User.Identity.Name;

            var siteQuery =
                from a in db.UserSites
                where (a.SiteID == id)
                select a;

            var userQuery =
                from a in db.Users
                where (a.Username == username)
                select a;

            int userID = 0;
            foreach (var elem in userQuery)
            {
                userID = elem.ID;
            }
            foreach (var elem in siteQuery)
            {
                if (userID == elem.UserID)
                    return true;
            }*/
            foreach (Site site in SitesForUser())
                if (site.ID == id)
                    return true;

            return false;
        }

        public ActionResult Daily(int id = 0)
        {
            if (id == 0)
            {
                return RedirectToAction("ChooseSite");
            }

            if (!UserAuth(id))
            {
                return new HttpStatusCodeResult(HttpStatusCode.Unauthorized);
            }

            var definition =
                from a in db.CheckDefs
                where (a.SiteID == id && a.Active == 1)
                orderby a.OrderNum
                select a;

            List<CheckDef> defList = new List<CheckDef>();
            List<CheckElement> elemList = new List<CheckElement>();

            List<int> listID = new List<int>();
            foreach (var elem in definition)
                listID.Add(elem.ID);

            var elements =
                from a in db.CheckElements
                where (listID.Contains(a.CheckID))
                select a;

            foreach (var elem in definition)
                defList.Add(elem);
            foreach (var elem in elements)
                if (elem.Timestamp.Date == DateTime.Now.Date)
                    elemList.Add(elem);

            Dictionary<string, object> elemMap = new Dictionary<string, object>();
            elemMap["definitions"] = defList;
            elemMap["elements"] = elemList;
            elemMap["sitename"] = SiteName(id);
            elemMap["siteid"] = id;

            //DateTime cutoff = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 12, 0, 0);
            DateTime cutoff = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 8, 0, 0);
            ViewBag.cutoff = cutoff;

            return View(elemMap);
        }

        public ActionResult ChooseSite()
        {
            /*var sites = db.Sites.Where(x => x.Active == true);
            int userID = UserID(User.Identity.Name);
            var usersites = db.UserSites.Where(x => x.UserID == userID);
            List<Site> list = new List<Site>();
            foreach (var s in sites)
            {
                foreach (var u in usersites)
                {
                    if (s.ID == u.SiteID)
                        list.Add(s);
                }
            }*/
            List<Site> list = SitesForUser();
            return View(list);
        }

        private void DeleteInternal(int id)
        {
            CheckElement checkelement = db.CheckElements.Find(id);
            db.CheckElements.Remove(checkelement);
            db.SaveChanges();
        }

        private int CheckElementsContains(int id)
        {
            var query =
                from a in db.CheckElements
                where (a.CheckID == id)
                select a;
            foreach (var elem in query)
            {
                if (elem.Timestamp.Date == DateTime.Now.Date)
                    return elem.ID;
            }
            return 0;
        }

        private CheckElement CEEntry(DateTime dt, List<CheckElement> list)
        {
            CheckElement element = new CheckElement();
            foreach (CheckElement elem in list) {
                if (elem.Timestamp.Date == dt.Date) {
                    element = elem;
                    return element;
                }
            }
            return null;
        }

        private bool ElemSame(CheckElement elem, int pos)
        {
            var query =
                from a in db.CheckElements
                where (a.ID == pos)
                select a;
            foreach (var e in query)
            {
                if (e.Status == elem.Status && e.Comment == elem.Comment)
                return true;
            }
            return false;
        }

        private void SubmitLateReason(int id, string reason)
        {
            int userID = UserID(User.Identity.Name.ToLower());
            LateReason lreason = new LateReason();
            lreason.SiteID = id;
            lreason.UserID = userID;
            lreason.Reason = reason;
            lreason.Timestamp = DateTime.Now;
            DateTime start = DateTime.Now.Date;
            DateTime end = start.AddDays(1);
            var query =
                from a in db.LateReasons
                where (a.SiteID == id && a.Timestamp >= start && a.Timestamp < end)
                select a;

            //List<LateReason> existLR = query.ToList();        
            if (ModelState.IsValid)
            {
                foreach (var elem in query)
                    db.LateReasons.Remove(elem);
                db.SaveChanges();
                db.LateReasons.Add(lreason);
                db.SaveChanges();
            }
        }
        
        public ActionResult Submit(int id, string checks, string comments, string latereason)
        {
            //if (id == 0 || String.IsNullOrEmpty(checks) || String.IsNullOrEmpty(comments))
                //return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            if (!String.IsNullOrEmpty(latereason))
                SubmitLateReason(id, latereason);
            string delim = "~!*";
            List<CheckDef> defs = new List<CheckDef>();
            List<CheckElement> elems = new List<CheckElement>();
            comments = comments.Replace(delim, "^");
            List<string> commentList = comments.Split('^').ToList<string>();

            var dquery =
                from a in db.CheckDefs
                where (a.SiteID == id && a.Active == 1)
                orderby (a.OrderNum)
                select a;

            foreach (var elem in dquery)
                defs.Add(elem);


            //if (defs.Count != checks.Length)
                //return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            int userID = UserID(User.Identity.Name.ToLower());
            var pos = 0;
            for (int i = 0; i < defs.Count; i++)
            {
                CheckElement elem = new CheckElement();
                elem.CheckID = defs[i].ID;
                elem.UserID = UserID(User.Identity.Name.ToLower());
                elem.Timestamp = DateTime.Now;
                if (checks[i] == 'G')
                    elem.Status = 1;
                if (checks[i] == 'B')
                    elem.Status = 2;
                elem.Comment = commentList[i];
                if (elem.Status == 1 || elem.Status == 2)
                {
                    elems.Add(elem);
                    pos = CheckElementsContains(elem.CheckID);
                    if (pos != 0 && !ElemSame(elem, pos))
                    {
                        DeleteInternal(pos);
                    }
                        
                }
            }

            string o = "";
            foreach (var elem in elems) {
                o = o + elem.CheckID + " " + elem.UserID + " " + elem.Timestamp + " " + elem.Status + " " + elem.Comment + "<br />";
            }

            bool success = false;
            if (ModelState.IsValid)
            {
                foreach (var elem in elems) {
                    pos = CheckElementsContains(elem.CheckID);
                    if (pos == 0)
                        db.CheckElements.Add(elem);
                }
                db.SaveChanges();
                success = true;
            }

            

            return RedirectToAction("View");
        }

        private bool AuthorizedUserForSite(int id)
        {
            /*var sites = db.Sites.Where(x => x.Active == true);
            int userID = UserID(User.Identity.Name);
            var usersites = db.UserSites.Where(x => x.UserID == userID);
            List<Site> list = new List<Site>();
            foreach (var s in sites)
            {
                foreach (var u in usersites)
                {
                    if (s.ID == u.SiteID)
                        list.Add(s);
                }
            }

            if (list.Count == 0)
                return false;
            return true;*/
            foreach (Site site in SitesForUser())
                if (site.ID == id)
                    return true;
            return false;
        }

        private List<Site> AdminSitesForUser()
        {
            var sites = db.Sites.Where(x => x.Active == true);
            int userID = UserID(User.Identity.Name.ToLower());
            var usersites = db.UserSites.Where(x => x.UserID == userID);
            List<Site> list = new List<Site>();
            foreach (var s in sites)
            {
                foreach (var u in usersites)
                {
                    if (s.ID == u.SiteID && Flags.ContainsBit(u.Flags, Flags.FLAG_SITEADMIN))
                        list.Add(s);
                }
            }

            var ugroups =
                from a in db.UserGroups
                where (a.User.Username.ToLower() == User.Identity.Name.ToLower())
                select a;

            List<UserGroup> ugroupList = ugroups.ToList();

            var sgroups =
                from a in db.GroupSites
                select a;

            foreach (var sgroup in sgroups)
            {
                if (!list.Contains(sgroup.Site) && UserMemberOfGroup(ugroupList) && Flags.ContainsBit(sgroup.Flags, Flags.FLAG_SITEADMIN))
                {
                    list.Add(sgroup.Site);
                }
            }
            return list;
        }

        private List<Site> SitesForUser()
        {
            var sites = db.Sites.Where(x => x.Active == true);
            int userID = UserID(User.Identity.Name.ToLower());
            var usersites = db.UserSites.Where(x => x.UserID == userID);
            List<Site> list = new List<Site>();
            foreach (var s in sites)
            {
                foreach (var u in usersites)
                {
                    if (s.ID == u.SiteID)
                        list.Add(s);
                }
            }

            var ugroups =
                from a in db.UserGroups
                where (a.User.Username == User.Identity.Name.ToLower())
                select a;

            List<UserGroup> ugroupList = ugroups.ToList();

            var sgroups =
                from a in db.GroupSites
                select a;

            foreach (var sgroup in sgroups)
            {
                if (!list.Contains(sgroup.Site) && UserMemberOfGroup(ugroupList))
                {
                    list.Add(sgroup.Site);
                }
            }
            return list;
        }

        public ActionResult View()
        {
            List<Site> list = SitesForUser();

            if (list.Count == 0)
                return new HttpStatusCodeResult(HttpStatusCode.Unauthorized);

            return View(list);
        }

        private bool UserMemberOfGroup(List<UserGroup> ugroupList)
        {
            foreach (UserGroup ugroup in ugroupList)
                if (ugroup.User.Username.ToLower() == User.Identity.Name.ToLower())
                    return true;
            return false;
        }

        public void safeAdd(Dictionary<DateTime, string> map, DateTime key, string value)
        {
            if (!map.ContainsKey(key))
                map.Add(key, value);
        }

        private CheckDef findParent(CheckDef elem, List<CheckDef> defs)
        {
            int parentID = elem.Parent;
            if (parentID == 0)
                return null;

            CheckDef parent = new CheckDef();
            for (int i = 0; i < defs.Count; i++)
                if (defs[i].ID == parentID)
                {
                    parent = defs[i];
                    break;
                }

            return parent;
        }

        private string FullFamily(CheckDef celem, List<CheckDef> defList)
        {
            string comments = "";
            Stack<CheckDef> stack = new Stack<CheckDef>();


            while (celem != null)
            {
                stack.Push(celem);
                celem = findParent(celem, defList);
            }

            while (stack.Count > 0)
            {
                comments = comments + stack.Peek().Name + "\\";
                stack.Pop();
            }
            return comments.Substring(0, comments.Length - 1);
        }

        private CheckDef defID(int id, List<CheckDef> defs)
        {
            foreach (CheckDef def in defs)
            {
                if (id == def.ID)
                    return def;
            }
            return null;
        }

        public ActionResult ViewDetail(int id, DateTime start, DateTime end)
        {
            if (!AuthorizedUserForSite(id))
                return new HttpStatusCodeResult(HttpStatusCode.Unauthorized);
            end = end.AddDays(1);
            var defQuery =
                from a in db.CheckDefs
                where (a.SiteID == id && a.Active == 1)
                orderby a.OrderNum
                select a;
            
            List<CheckDef> defList = new List<CheckDef>();
            foreach (var elem in defQuery)
                defList.Add(elem);

            var elemQuery =
                from a in db.CheckElements
                where (a.Timestamp > start && a.Timestamp <= end)
                select a;

            List<CheckElement> elemList = new List<CheckElement>();
            bool found = false;
            foreach (var elem in elemQuery)
            {
                found = false;
                foreach (var e in defList)
                {
                    if (e.ID == elem.CheckID)
                    {
                        found = true;
                    }
                }
                if (found)
                    elemList.Add(elem);
            }

            //string s = "";
            /*foreach (CheckElement elem in elemList) {
                s = s + elem.CheckID + " " + elem.Timestamp + "<br />";
            }*/

            Dictionary<int, string> lines = new Dictionary<int, string>();
            //List<Dictionary<DateTime, string>> elemDTMap = new List<Dictionary<DateTime, string>>();
            Dictionary<int, Dictionary<DateTime, string>> elemDTMap = new Dictionary<int, Dictionary<DateTime, string>>();
            Dictionary<int, Dictionary<DateTime, string>> elemUserMap = new Dictionary<int, Dictionary<DateTime, string>>();
            Dictionary<int, Dictionary<DateTime, string>> elemCommentMap = new Dictionary<int, Dictionary<DateTime, string>>();
            

            foreach (CheckDef def in defList)
            {
                string line = "";
                Dictionary<DateTime, string> dtmap = new Dictionary<DateTime, string>();
                Dictionary<DateTime, string> usermap = new Dictionary<DateTime, string>();
                Dictionary<DateTime, string> commentMap = new Dictionary<DateTime, string>();
                List<DateTime> dtList = new List<DateTime>();
                foreach (CheckElement elem in elemList)
                    if (def.ID == elem.CheckID)
                        dtList.Add(elem.Timestamp.Date);
                int count = 0;
                while (start.AddDays(count).Date <= end.AddDays(-1).Date)
                {
                    if (!dtList.Contains(start.AddDays(count).Date))
                        dtList.Add(start.AddDays(count).Date);
                    count++;
                }
                /*
                for (DateTime i = start.Date; i <= end.Date; i.AddDays(1))
                {
                    if (!dtList.Contains(i))
                        dtList.Add(i);
                }*/
                dtList.Sort();

                /*foreach (DateTime dt in dtList)
                {
                    CheckElement elem = CEEntry(dt, elemList);
                    if (elem == null)
                    {
                        line += "E";
                    }
                    else
                    {
                        if (elem.Status == 1)
                            line += "G";
                        else if (elem.Status == 2)
                            line += "B";
                        else
                            line += "E";
                    }
                }*/

                

                foreach (CheckElement element in elemList)
                {
                    


                    if (element.CheckID == def.ID && def.Level >= 2)
                    {
                        

                        if (element.Status == 1)
                            line += "G";
                        else if (element.Status == 2)
                            line += "B";
                        else
                            line += "E";

                        if (element.Status == 1)
                                safeAdd(dtmap, element.Timestamp.Date, "G");
                            else if (element.Status == 2)
                                safeAdd(dtmap, element.Timestamp.Date, "B");
                            else
                                safeAdd(dtmap, element.Timestamp.Date, "E");

                        if (element.Status == 1 || element.Status == 2)
                        {
                            safeAdd(usermap, element.Timestamp.Date, element.User.Username);
                            safeAdd(commentMap, element.Timestamp.Date, element.Comment);
                        }
                            
                    }
                    
                    List<DateTime> keys = new List<DateTime>();
                    

                }

                foreach (DateTime dt in dtList)
                {
                    safeAdd(dtmap, dt, "");
                    safeAdd(usermap, dt, "");
                }

                //lines.Add(def.ID, line);
                //elemDTMap.Add(dtmap);
                elemDTMap.Add(def.ID, dtmap);
                elemUserMap.Add(def.ID, usermap);
                elemCommentMap.Add(def.ID, commentMap);

                lines[def.ID] = line;
            }

            Dictionary<int, CheckDef> defMap = new Dictionary<int, CheckDef>();
            foreach (CheckDef elem in defList)
            {
                defMap[elem.ID] = elem;
            }

            
            /*CheckDef celem = defList[11];
            string comments = FullFamily(celem, defList);*/
            string comments = "";

            foreach (CheckElement elem in elemList)
            {
                if (!String.IsNullOrEmpty(elem.Comment))
                {
                    CheckDef cdef = defID(elem.CheckID, defList);
                    string family = FullFamily(cdef, defList);
                    comments = comments + family + "\n" + elem.Timestamp.ToString() + " by " + elem.User.Username + "\n" + elem.Comment + "\n\n";
                }
            }

            

            HCReport report = new HCReport();
            report.siteid = id;
            report.sitename = SiteName(id);
            report.startdate = start;
            report.enddate = end.AddDays(-1);
            report.defList = defList;
            report.elemList = elemList;
            report.results = lines;
            //report.comments = "Comments go here.\nPlease go get some comments.";
            report.comments = comments;
            //report.comments = report.comments.Replace("\n", "<br>");
            report.defMap = defMap;
            report.elemDTMap = elemDTMap;
            report.elemUserMap = elemUserMap;
            report.elemCommentMap = elemCommentMap;

            List<string> usernames = new List<string>();
            /*var uq =
                from a in db.Users
                select a.Username;

            

            foreach (var elem in uq)
                usernames.Add(elem);*/
            
            var uQuery =
                from a in db.CheckElements
                where (a.Timestamp >= start && a.Timestamp <= end && a.CheckDef.SiteID == id)
                select a.User.Username;

            foreach (var elem in uQuery)
                if (!usernames.Contains(elem))
                    usernames.Add(elem);

            report.usernames = usernames;

            var dquery =
                from a in db.CheckElements
                where (a.CheckDef.SiteID == id && a.CheckDef.Active == 1 && a.Timestamp > start && a.Timestamp <= end)
                select a;

            Dictionary<DateTime, DateTime> lastUpdate = new Dictionary<DateTime, DateTime>();
            Dictionary<DateTime, List<DateTime>> timestampMap = new Dictionary<DateTime, List<DateTime>>();
            foreach (var d in dquery)
            {
                if (timestampMap.Keys.Contains(d.Timestamp.Date))
                    timestampMap[d.Timestamp.Date].Add(d.Timestamp);
                else
                {
                    List<DateTime> list = new List<DateTime>();
                    list.Add(d.Timestamp);
                    //timestampMap[d.Timestamp.Date].Add(d.Timestamp);
                    timestampMap.Add(d.Timestamp.Date, list);
                }
            }

            foreach (DateTime key in timestampMap.Keys)
            {
                DateTime min = timestampMap[key].Min<DateTime>();
                DateTime max = timestampMap[key].Max<DateTime>();
                lastUpdate.Add(key, max);
            }

            report.lastUpdate = lastUpdate;

            return View(report);
        }

        public string TestData(int days = 0)
        {
            if (days == 0)
                return "/TestData/Days";
            var defs =
                from a in db.CheckDefs
                where (a.SiteID == 1)
                select a;
            List<CheckElement> elems = new List<CheckElement>();
            List<int> excludes = new List<int>();
            excludes.Add(15);
            excludes.Add(16);
            Random r = new Random();
            for (int day = 1; day <= days; day++)
            {
                if (!excludes.Contains(day))
                {
                    foreach (var def in defs)
                    {
                        CheckElement elem = new CheckElement();
                        elem.Timestamp = DateTime.Now.AddDays(-day);
                        elem.CheckID = def.ID;
                        elem.Status = r.Next(1, 3);
                        elem.UserID = 1;
                        if (r.Next(1, 51) == 1)
                            elem.Comment = "comment";
                        elems.Add(elem);
                    }
                }
                
            }

            bool success = false;
            if (ModelState.IsValid)
            {
                foreach (var elem in elems)
                {
                    db.CheckElements.Add(elem);
                }
                db.SaveChanges();
                success = true;
            }

            return "Success.";
        }

        public ActionResult ModifySiteChoose()
        {
            /*List<Site> sites = new List<Site>();

            var query =
                from a in db.UserSites
                where (a.User.Username == User.Identity.Name)
                orderby a.Site.Name
                select a;

            foreach (UserSite site in query)
                if (!sites.Contains(site.Site) && Flags.ContainsBit(site.Flags, Flags.FLAG_SITEADMIN))
                    sites.Add(site.Site);*/
            List<Site> sites = AdminSitesForUser();

            return View(sites);
        }

        private List<User> UsersForSite(int id)
        {
            List<User> allUsers = new List<User>();
            List<User> outUsers = new List<User>();
            List<int> map = new List<int>();
            var uQuery =
                from a in db.Users
                select a;

            var mapQuery =
                from a in db.UserSites
                where (a.SiteID == id)
                select a;

            foreach (var u in uQuery)
                allUsers.Add(u);

            foreach (var m in mapQuery)
            {
                map.Add(m.UserID);
            }

            var oQuery =
                from a in db.Users
                where (map.Contains(a.ID))
                select a;

            foreach (var o in oQuery)
                outUsers.Add(o);

            return outUsers;
        }

        private List<CheckDef> DefsForSite(int id, bool active = false)
        {
            List<CheckDef> defs = new List<CheckDef>();
            var query =
                from a in db.CheckDefs
                where (a.SiteID == id)
                orderby a.OrderNum
                select a;
            foreach (var e in query)
            {
                if (!active)
                    defs.Add(e);
                else if (active && e.Active == 1)
                    defs.Add(e);
            }
            return defs;
        }

        private string DefString(List<CheckDef> defs)
        {
            string s = "";

            foreach (CheckDef def in defs)
            {
                s = s + FullFamily(def, defs) + "\n";
            }

            return s;
        }

        private string DefStringWithOrder(List<CheckDef> defs)
        {
            string s = "";

            foreach (CheckDef def in defs)
            {
                s = s + def.OrderNum + " " + FullFamily(def, defs) + "\n";
            }

            return s;
        }

        private string RemoveChar(string input, char ch)
        {
            string s = "";
            for (int i = 0; i < input.Length; i++)
            {
                if (input[i] != ch)
                    s += input[i];
            }
            return s;
        }

        private int defFindID(string name, List<CheckDef> defs)
        {
            foreach (CheckDef def in defs)
                if (name == def.Name)
                    return def.ID;
            return 0;
        }

        private CheckDef defFromID(int id, List<CheckDef> defs)
        {
            for (int i = 0; i < defs.Count; i++)
                if (defs[i].ID == id)
                    return defs[i];
            return null;
        }



        private int MaxID(List<CheckDef> defs)
        {
            int max = 0;
            foreach (CheckDef def in defs)
                if (def.ID > max)
                    max = def.ID;
            return max;
        }

        private int MaxLevel(List<DefInfo> defs)
        {
            int max = 0;
            foreach (DefInfo def in defs)
                if (def.level > max)
                    max = def.level;
            return max;
        }

        private DefInfo findElement(string name, int level, List<DefInfo> defs)
        {
            foreach (DefInfo def in defs)
            {
                if (def.shortName == name && def.level == level)
                    return def;
            }
            return null;
        }

        private void updateDefList(DefInfo overwrite, List<CheckDef> defList) {
            CheckDef ndef = new CheckDef();
            CheckDef target = null;
            foreach (CheckDef def in defList)
            {
                if (def.Name == overwrite.shortName && def.Level == overwrite.level && def.OrderNum == overwrite.orderNum)
                {
                    target = def;
                    defList.Remove(def);
                    break;
                }
                    
            }
            if (target != null)
            {
                ndef.ID = target.ID;
                ndef.SiteID = target.SiteID;
                ndef.Name = overwrite.shortName;
                ndef.Level = overwrite.level;
                ndef.Parent = overwrite.parentID;
                ndef.OrderNum = overwrite.orderNum;
                defList.Add(ndef);
            }
            string shortname = overwrite.shortName;
            string longname = overwrite.fullName;
            string parent = overwrite.parentName;

            int a = 55;
        }

        private DefInfo GenerateDefInfo(CheckDef elem, List<CheckDef> defList)
        {
            DefInfo def = new DefInfo();
            def.shortName = elem.Name;
            def.fullName = FullFamily(elem, defList);
            def.parentID = elem.Parent;
            def.orderNum = elem.OrderNum;
            List<string> nameList = def.fullName.Split('\\').ToList<string>();
            if (nameList.Count == 1)
                def.parentName = "";
            else
                def.parentName = nameList[nameList.Count - 2];
            def.level = (nameList.Count - 1);
            return def;
        }

        private List<DefInfo> GenerateDefInfoList(List<CheckDef> defList)
        {
            List<DefInfo> generated = new List<DefInfo>();
            foreach (CheckDef elem in defList)
            {
                DefInfo def = new DefInfo();
                def.shortName = elem.Name;
                def.fullName = FullFamily(elem, defList);
                def.parentID = elem.Parent;
                def.orderNum = elem.OrderNum;
                List<string> nameList = def.fullName.Split('\\').ToList<string>();
                if (nameList.Count == 1)
                    def.parentName = "";
                else
                    def.parentName = nameList[nameList.Count - 2];
                def.level = (nameList.Count - 1);
                generated.Add(def);
            }
            return generated;
        }

        private int LookupFullName(string fullname, List<DefInfo> defs)
        {
            for (int i = 0; i < defs.Count; i++)
                if (defs[i].fullName == fullname)
                    return i;
            return 0;
        }

        private int LookupFullName(string fullname, List<CheckDef> defs)
        {
            for (int i = 0; i < defs.Count; i++)
                if (FullFamily(defs[i], defs) == fullname)
                    return i;
            return 0;
        }

        private int LookupStringPos(List<string> list, string target)
        {
            for (int i = 0; i < list.Count; i++)
            {
                if (list[i] == target)
                    return i;
            }
            return 0;
        }

        private int CheckDefPosFromID(int id, List<CheckDef> defs) {
            for (int i = 0; i < defs.Count; i++)
                if (defs[i].ID == id)
                    return i;
            return 0;
        }

        private CheckDef CheckDefFromFullName(string fullname, List<CheckDef> defs)
        {
            foreach (CheckDef def in defs)
                if (FullFamily(def, defs) == fullname)
                    return def;
            return null;
        }

        private bool IntersectsDefInfoInfo(List<DefInfo> a, List<DefInfo> b)
        {
            foreach (DefInfo suba in a)
                foreach (DefInfo subb in b)
                    if (suba.fullName == subb.fullName)
                        return true;
            return false;
        }

        //private List<string> FullNameList(List<DefInfo> defs)
        //{
        //    List<string> names = new List<string>();
        //    foreach (DefInfo def in defs)
        //        names.Add(FullFamily(def, defs));
        //    return names;
        //}

        private int CountImmediateChildren(CheckDef parent, List<CheckDef> defs)
        {
            int count = 0;

            foreach (CheckDef candidate in defs)
                if (FullFamily(candidate, defs).Contains(FullFamily(parent, defs)) && FullFamily(candidate, defs) != FullFamily(parent, defs))
                    count++;

            return count;
        }

        private int CountImmediateChildren(DefInfo parent, List<DefInfo> defs)
        {
            int count = 0;

            foreach (DefInfo candidate in defs)
            {
                if (candidate.fullName.Contains(parent.fullName) && candidate.fullName != parent.fullName)
                    count++;
            }

            return count;
        }

        public ActionResult UpdateDefinition(int id, string definition)
        {
            SiteInfo info = new SiteInfo();
            string defstring = "";
            List<DefInfo> defInfoList = new List<DefInfo>();
            int count = 0;
            definition = RemoveChar(definition, '\r');
            List<CheckDef> defList = DefsForSite(id, true);
            List<DefInfo> dbList = new List<DefInfo>();
            List<DefInfo> additions = new List<DefInfo>();

            List<string> defStringList = definition.Split('\n').ToList<string>();

            //  Populate dbList with live contents.
            foreach (CheckDef elem in defList)
            {
                DefInfo def = new DefInfo();
                def.shortName = elem.Name;
                def.fullName = FullFamily(elem, defList);
                def.parentID = elem.Parent;
                def.orderNum = elem.OrderNum;
                List<string> nameList = def.fullName.Split('\\').ToList<string>();
                if (nameList.Count == 1)
                    def.parentName = "";
                else
                    def.parentName = nameList[nameList.Count - 2];
                def.level = (nameList.Count - 1);
                
                dbList.Add(def);
            }

            //foreach (DefInfo elem in dbList)
            //{
            //    if (CountImmediateChildren(elem, dbList) == 0)
            //        elem.level = 2;
            //}

            

            foreach (string line in definition.Split('\n').ToList<string>())
            {
                if (!String.IsNullOrEmpty(line) && !String.IsNullOrWhiteSpace(line))
                {
                    count++;
                    DefInfo def = new DefInfo();
                    def.fullName = line;
                    List<string> names = line.Split('\\').ToList<string>();
                    def.level = (names.Count - 1);
                    def.orderNum = count * 100;
                    def.shortName = names[names.Count - 1];
                    if (names.Count == 1)
                        def.parentName = "";
                    else
                        def.parentName = names[names.Count - 2];
                    def.parentID = defFindID(def.parentName, defList);
                    defInfoList.Add(def);
                }
            }

            List<string> dbFullNamesList = new List<string>();
            //Populate dbFullNamesList from dbList.
            foreach (DefInfo elem in dbList)
            {
                dbFullNamesList.Add(elem.fullName);
            }

            // Generate disable list.
            List<CheckDef> disable = new List<CheckDef>();
            foreach (CheckDef def in defList)
            {
                if (!defStringList.Contains(FullFamily(def, defList))) {
                    disable.Add(def);
                }
            }

            //  Commit disable list to db.

            if (ModelState.IsValid)
            {
                foreach (CheckDef def in disable)
                {
                    var q =
                        from a in db.CheckDefs
                        where (a.ID == def.ID)
                        select a;
                    foreach (var e in q)
                    {
                        e.Active = 0;
                        break;
                    }
                }
                db.SaveChanges();
            }

            //Generate additions list.
            foreach (DefInfo elem in defInfoList)
            {
                if (!dbFullNamesList.Contains(elem.fullName))   //  New entry.
                {
                    additions.Add(elem);
                }
            }
            List<CheckDef> alldefList = DefsForSite(id, false);
            //  Check additions list for any entries that can be re-enabled.
            List<DefInfo> enableList = new List<DefInfo>();
            foreach (DefInfo elem in additions)
            {
                CheckDef dbe = CheckDefFromFullName(elem.fullName, alldefList);
                if (dbe != null && dbe.Active == 0)
                {
                    enableList.Add(GenerateDefInfo(dbe, alldefList));
                }
            }

            foreach (DefInfo elem in enableList)
                additions.RemoveAt(LookupFullName(elem.fullName, additions));

            //  Commit re-enabled list to db.

            List<CheckDef> enableDefList = new List<CheckDef>();
            foreach (DefInfo elem in enableList)
            {
                string fn = elem.fullName;
                int pos = LookupFullName(elem.fullName, alldefList);
                CheckDef def = alldefList[pos];
                enableDefList.Add(def);
            }


            foreach (CheckDef def in enableDefList)
            {
                var eQuery =
                    from a in db.CheckDefs
                    where (a.ID == def.ID)
                    select a;
                //int mcount = 0;
                foreach (var e in eQuery)
                {
                    e.Active = 1;
                    break;
                }
            }
            db.SaveChanges();
            
            int b = 55;
            /*
             * Insert additions into db.
             * Generate new ParentIDs.
             * Update all ParentIDs in db.
             * Update all OrderNums in db.
             */

            //  Insert additions into db.
            if (ModelState.IsValid)
            {
                foreach (DefInfo def in additions)
                {
                    CheckDef ndef = new CheckDef();
                    ndef.ID = 0;
                    ndef.SiteID = id;
                    ndef.Name = def.shortName;
                    ndef.Level = def.level;
                    ndef.Parent = def.parentID;
                    ndef.OrderNum = def.orderNum;
                    //db.Entry(ndef).State = EntityState.Modified;
                    //db.CheckElements.Add(checkelement);
                    db.CheckDefs.Add(ndef);
                }
                db.SaveChanges();
            }

            //  Refresh defList & dbList from db in order to generate correct ParentIDs.
            defList.Clear();
            defList = DefsForSite(id);
            dbList.Clear();
            foreach (CheckDef elem in defList)
            {
                DefInfo def = new DefInfo();
                def.shortName = elem.Name;
                def.fullName = FullFamily(elem, defList);
                def.parentID = elem.Parent;
                def.orderNum = elem.OrderNum;
                List<string> nameList = def.fullName.Split('\\').ToList<string>();
                if (nameList.Count == 1)
                    def.parentName = "";
                else
                    def.parentName = nameList[nameList.Count - 2];
                def.level = (nameList.Count - 1);
                dbList.Add(def);
            }

            //  Generate ParentID updates.
            foreach (DefInfo elem in additions)
            {
                /*int dbID = defFindID(elem.fullName, defList);
                CheckDef dbDef = defFromID(dbID, defList);
                elem.parentID = dbDef.Parent;*/
            }

            List<CheckDef> cdeflist = defList;
            int maxLevel = MaxLevel(additions);
            for (int level = 0; level <= maxLevel; level++)
            {
                foreach (DefInfo elem in additions)
                {
                    //List<string> names = elem.fullName.Split('\\').ToList<string>();
                    if (level == 0)
                    {
                        if (elem.level == level)
                            elem.parentID = 0;
                    }
                    else
                    {
                        if (elem.level == level)
                        {
                            DefInfo probableParent = findElement(elem.parentName, level - 1, dbList);
                            elem.parentID = defFindID(probableParent.shortName, cdeflist);
                            updateDefList(elem, cdeflist);
                            dbList = GenerateDefInfoList(cdeflist);
                            //  Need to update defList (or a copy of defList).
                            int a = 55;
                        }
                    }
                }
            }
            defList = cdeflist;
            //  Commit ParentID updates and set active to 1 to db.
            if (ModelState.IsValid)
            {
                foreach (DefInfo def in additions)
                {
                    CheckDef ndef = new CheckDef();
                    int dbID = defFindID(def.fullName, defList);
                    CheckDef dbDef = defFromID(dbID, defList);
                    /*ndef.ID = dbDef.ID;
                    ndef.SiteID = id;
                    ndef.Name = def.shortName;
                    ndef.Level = def.level;
                    ndef.Parent = def.parentID;
                    ndef.OrderNum = def.orderNum;
                    db.Entry(ndef).State = EntityState.Modified;*/

                    var query =
                        from a in db.CheckDefs
                        where (a.Name == def.shortName && a.Level == def.level && a.SiteID == id && a.OrderNum == def.orderNum)
                        select a;
                    int ucount = 0;
                    foreach (var elem in query)
                    {
                        if (ucount == 1)
                            break;
                        elem.Parent = def.parentID;
                        elem.Active = 1;
                        ucount++;
                    }

                    
                    /*db.Entry(checkelement).State = EntityState.Modified;
                    db.SaveChanges();*/
                }
                db.SaveChanges();
            }
            

            //  Generate OrderNum updates.
            List<CheckDef> reorderList = new List<CheckDef>();

            List<CheckDef> cDB = new List<CheckDef>();
            var cquery =
                from a in db.CheckDefs
                where (a.SiteID == id)
                orderby a.OrderNum
                select a;

            foreach (var elem in cquery)
                cDB.Add(elem);

            List<DefInfo> iDB = GenerateDefInfoList(cDB);
            /*for (int i = 0; i < defStringList.Count; i++)
            {
                int pos = LookupFullName(defStringList[i], iDB);
                if (pos != 0)
                    iDB[pos].level = i * 100;
            }

            for (int i = 0; i < cDB.Count; i++)
            {
                if (cDB[i].OrderNum != iDB[i].orderNum)
                    cDB[i].OrderNum = iDB[i].orderNum;
            }*/

            foreach (CheckDef def in cDB)
            {
                int pos = LookupStringPos(defStringList, FullFamily(def, cDB));
                if (def.OrderNum != ((pos + 1) * 100))
                {
                    def.OrderNum = ((pos + 1) * 100);
                }
            }

            List<CheckDef> nDB = new List<CheckDef>();
            //  Update ordernums to db.
            if (ModelState.IsValid)
            {
                var q =
                    from a in db.CheckDefs
                    where (a.SiteID == id)
                    orderby id
                    select a;

                foreach (var e in q)
                {
                    CheckDef check = defFromID(e.ID, cDB);
                    if (check.OrderNum != e.OrderNum)
                    {
                        e.OrderNum = check.OrderNum;
                    }
                }
                db.SaveChanges();
                
                var n =
                    from a in cDB
                    orderby a.OrderNum
                    select a;

                foreach (var e in n)
                    nDB.Add(e);
            }

            //  Update levels.
            /*List<DefInfo> newdeflist = GenerateDefInfoList(DefsForSite(id, true));
            foreach (DefInfo def in newdeflist)
            {
                if (CountImmediateChildren(def, newdeflist) == 0 && def.level < 2)
                    def.level = 2;
            }*/

            List<CheckDef> newdeflist = DefsForSite(id, true);
            foreach (CheckDef def in newdeflist)
            {
                if (CountImmediateChildren(def, newdeflist) == 0 && def.Level < 2)
                    def.Level = 2;
            }

            if (ModelState.IsValid)
            {
                var lquery =
                    from a in db.CheckDefs
                    where (a.ID == id && a.Active == 1)
                    select a;
                foreach (var elem in lquery)
                {
                    int pos = CheckDefPosFromID(elem.ID, newdeflist);
                    if (pos != 0 && elem.Level != newdeflist[pos].Level)
                        elem.Level = newdeflist[pos].Level;
                }
                db.SaveChanges();
            }

            string ds = DefStringWithOrder(nDB);

            
            defstring += "**************\nNew Defs\n*************\n";
            defstring += ds;

            //foreach (DefInfo def in defInfoList)
            //{
            //    string elem = def.orderNum.ToString() + " " + def.parentID.ToString() + " " + def.shortName + " " + def.parentName + "\n";
            //    defstring += elem;
            //}
            defstring += "**********\nAdditions:\n";
            foreach (DefInfo def in additions)
            {
                string elem = def.orderNum.ToString() + " " + def.parentID.ToString() + " " + def.shortName + " " + def.parentName + "\n";
                defstring += elem;
            }

            info.defstring = defstring + "\n\n\n" + definition;
            return View(info);
        }

        private Dictionary<string, int> FlagsForUsersForSite(int siteid)
        {
            Dictionary<string, int> flags = new Dictionary<string, int>();

            var query =
                from a in db.UserSites
                where (a.SiteID == siteid)
                select a;

            foreach (var q in query)
                flags[q.User.Username] = q.Flags;

            return flags;
        }

        Dictionary<string, User> UserDictionaryFromDB()
        {
            Dictionary<string, User> d = new Dictionary<string, User>();
            //Dictionary<int, UserSite> usiteMap = UserSiteDictionaryFromDB(id);
            var query =
                from a in db.Users
                //where (usiteMap.Keys.Contains(a.ID))
                select a;
            foreach (var elem in query)
                d.Add(elem.Username, elem);
            return d;
        }

        Dictionary<int, UserSite> UserSiteDictionaryFromDB(int id)
        {
            Dictionary<int, UserSite> d = new Dictionary<int, UserSite>();
            var query =
                from a in db.UserSites
                where (a.SiteID == id)
                select a;
            foreach (var elem in query)
                d.Add(elem.UserID, elem);
            return d;
        }

        Dictionary<string, UserSite> UserSiteDictionaryFromDBUName(int id)
        {
            Dictionary<string, UserSite> d = new Dictionary<string, UserSite>();
            var query =
                from a in db.UserSites
                where (a.SiteID == id)
                select a;
            foreach (var elem in query)
                d.Add(elem.User.Username, elem);
            return d;
        }

        public string UpdateUserSite(int id, List<UserInfo> users)
        {
            string o = "";
            foreach (UserInfo i in users)
            {
                o = o + i.username + " " + i.IsActive() + " " + i.IsAdmin() + " --- ";
            }

            Dictionary<string, User> userMap = UserDictionaryFromDB();
            Dictionary<int, UserSite> usiteMap = UserSiteDictionaryFromDB(id);
            Dictionary<string, UserSite> unameMap = UserSiteDictionaryFromDBUName(id);

            /* Additions:
             * Not in user:
             *      Add to user & usersite.
             * In user but not in usersite.
             *      Add to usersite.
             * 
             * Removals:
             *      Remove usersite.
             * 
             * Modification:
             *      Update usersite.
             */

            List<string> uadditions = new List<string>();
            List<string> usadditions = new List<string>();
            foreach (UserInfo user in users)
            {
                if (!userMap.Keys.Contains(user.username))
                {
                    uadditions.Add(user.username);
                    usadditions.Add(user.username);
                }
                else if (!unameMap.Keys.Contains(user.username))
                {
                    usadditions.Add(user.username);
                }
            }

            List<string> usremovals = new List<string>();
            foreach (UserInfo user in users)
            {
                if (!user.IsActive() && userMap.Keys.Contains(user.username))
                {
                    usremovals.Add(user.username);
                }
            }

            List<string> usmodify = new List<string>();
            foreach (UserInfo user in users)
            {
                if (unameMap.Keys.Contains(user.username) && (unameMap[user.username].Flags != user.Flags()))
                {
                    usmodify.Add(user.username);
                }
            }

            if (uadditions.Count > 0 || usadditions.Count > 0)
                WriteUsers(id, uadditions, usadditions, users);
            if (usremovals.Count > 0)
                RemoveUsers(id, usremovals);
            if (usmodify.Count > 0)
                ModifyUsers(id, usmodify, users);

            return o;
        }

        private void ModifyUsers(int id, List<string> usmodify, List<UserInfo> users)
        {
            Dictionary<string, UserInfo> uinfoMap = new Dictionary<string, UserInfo>();
            foreach (UserInfo user in users)
                uinfoMap.Add(user.username, user);
            var query =
                from a in db.UserSites
                where (a.SiteID == id && usmodify.Contains(a.User.Username))
                select a;
            if (ModelState.IsValid)
            {
                foreach (var q in query)
                {
                    q.Flags = uinfoMap[q.User.Username].Flags();
                }
                db.SaveChanges();
            }
            
        }

        private void RemoveUsers(int id, List<string> usremovals)
        {
            var query =
                from a in db.UserSites
                where (a.SiteID == id && usremovals.Contains(a.User.Username))
                select a;
            if (ModelState.IsValid)
            {
                foreach (var q in query)
                    db.UserSites.Remove(q);
                db.SaveChanges();
            }
        }

        private void WriteUsers(int id, List<string> uadditions, List<string> usadditions, List<UserInfo> users)
        {
            Dictionary<string, UserInfo> uinfoMap = new Dictionary<string, UserInfo>();
            foreach (UserInfo user in users)
                uinfoMap.Add(user.username, user);
            
            if (ModelState.IsValid)
            {
                foreach (string addition in uadditions)
                {
                    User newUser = new User();
                    newUser.Username = addition;
                    newUser.Active = true;
                    db.Users.Add(newUser);
                }
                db.SaveChanges();
                
            }

            var uquery =
                from a in db.Users
                where (uinfoMap.Keys.Contains(a.Username))
                select a;
            foreach (var u in uquery)
            {
                if (!u.Active)
                    u.Active = true;
            }

            Dictionary<string, User> userMap = UserDictionaryFromDB();
            if (ModelState.IsValid)
            {
                foreach (string addition in usadditions)
                {
                    UserSite newus = new UserSite();
                    newus.Flags = uinfoMap[addition].Flags();
                    newus.SiteID = id;
                    newus.UserID = userMap[addition].ID;
                    db.UserSites.Add(newus);
                }
                db.SaveChanges();
            }
        }

        public ActionResult ModifySite(int id = 0)
        {
            if (id == 0)
            {
                return RedirectToAction("ModifySiteChoose");
            }
            if (!UserAuth(id))
                return new HttpStatusCodeResult(HttpStatusCode.Unauthorized);

            SiteInfo info = new SiteInfo();

            info.siteid = id;
            info.sitename = SiteName(id);
            info.users = UsersForSite(id);
            info.defs = DefsForSite(id, true);
            info.defstring = DefString(info.defs);
            info.userFlags = FlagsForUsersForSite(id);

            return View(info);
        }

        private List<Site> AllActiveSites()
        {
            var usquery =
                from a in db.UserSites
                where (a.User.Username.ToLower() == User.Identity.Name.ToLower())
                select a.SiteID;

            var query =
                from a in db.Sites
                where (a.Active && usquery.Contains(a.ID))
                select a;
            return query.ToList();
        }

        private List<CheckElement> AllCheckElements(int id)
        {
            DateTime filter = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
            var query =
                from a in db.CheckElements
                where (a.CheckDef.SiteID == id && a.Timestamp >= filter)
                select a;
            return query.ToList();
        }

        private List<CheckElement> AllCheckElements(int id, int delta)
        {
            DateTime filter = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
            filter = filter.AddDays(-delta);
            DateTime tomorrow = filter.AddDays(1);
            var query =
                from a in db.CheckElements
                where (a.CheckDef.SiteID == id && a.Timestamp >= filter && a.Timestamp < tomorrow)
                select a;
            return query.ToList();
        }

        private List<CheckElement> AllUserCheckElements(int id, int delta)
        {
            DateTime filter = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
            filter = filter.AddDays(-delta);
            DateTime tomorrow = filter.AddDays(1);
            var query =
                from a in db.CheckElements
                where (a.CheckDef.SiteID == id && a.Timestamp >= filter && a.Timestamp < tomorrow)
                select a;
            List<CheckElement> elements = new List<CheckElement>();
            foreach (var elem in query)
                if (!SystemUserIDs().Contains(elem.ID))
                    elements.Add(elem);
            return elements;
        }

        private List<DateTime> GetTimeStamp(List<CheckElement> elements)
        {
            List<DateTime> list = new List<DateTime>();
            foreach (CheckElement elem in elements)
                list.Add(elem.Timestamp);
            return list;
        }

        DateTime MinDate(List<CheckElement> elements)
        {
            if (elements.Count == 0)
                return new DateTime(0);
            return GetTimeStamp(elements).Min();
        }

        DateTime MaxDate(List<CheckElement> elements)
        {
            if (elements.Count == 0)
                return new DateTime(0);
            return GetTimeStamp(elements).Max();
        }

        private int HCSummaryCompare(HCSummary a, HCSummary b)
        {
            if (a.lastUpdate == b.lastUpdate)
                return 0;
            else
            {
                if (a.lastUpdate.Ticks == 0)
                    return -1;
                else
                    return 1;
            }
            if (a.lastUpdate > b.lastUpdate)
                return -1;
            if (a.lastUpdate < b.lastUpdate)
                return 1;
            return 0;
        }

        public ActionResult Summary()
        {
            List<HCSummary> summaryList = new List<HCSummary>();
            
            List<Site> sites = SitesForUser();

            foreach (Site site in sites)
            {
                HCSummary summary = new HCSummary();
                summary.id = site.ID;
                summary.siteName = site.Name;
                List<CheckElement> checkList = AllCheckElements(site.ID);
                summary.firstUpdate = MinDate(checkList);
                summary.lastUpdate = MaxDate(checkList);
                summaryList.Add(summary);
            }

            summaryList.Sort(HCSummaryCompare);

            return View(summaryList);
        }

        public List<int> SystemUserIDs()
        {
            var query =
                from a in db.SystemUsers
                select a;
            List<int> userList = new List<int>();
            foreach (var elem in query)
                userList.Add(elem.ID);
            return userList;
        }

        public ActionResult SummaryMatrix(int delta = 7)
        {
            List<HCSummaryMatrix> summaryList = new List<HCSummaryMatrix>();

            List<Site> sites = SitesForUser();

            foreach (Site site in sites)
            {
                HCSummaryMatrix summary = new HCSummaryMatrix();
                summary.id = site.ID;
                summary.siteName = site.Name;
                Dictionary<int, Update> updateList = new Dictionary<int, Update>();
                for (int i = 0; i < delta; i++)
                {
                    List<CheckElement> checkList = AllUserCheckElements(site.ID, i);
                    Update upd = new Update();
                    upd.first = MinDate(checkList);
                    upd.last = MaxDate(checkList);
                    updateList.Add(i, upd);
                }
                summary.updateHistory = updateList;
                summaryList.Add(summary);
            }

            return View(summaryList);
        }

        // GET: /HealthChecks/
        public ActionResult Index()
        {
            var checkelements = db.CheckElements.Include(c => c.CheckDef).Include(c => c.User);
            return View(checkelements.ToList());
        }

        // GET: /HealthChecks/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CheckElement checkelement = db.CheckElements.Find(id);
            if (checkelement == null)
            {
                return HttpNotFound();
            }
            return View(checkelement);
        }

        // GET: /HealthChecks/Create
        public ActionResult Create()
        {
            ViewBag.CheckID = new SelectList(db.CheckDefs, "ID", "Name");
            ViewBag.UserID = new SelectList(db.Users, "ID", "Username");
            return View();
        }

        // POST: /HealthChecks/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,CheckID,Status,Comment,UserID,Timestamp")] CheckElement checkelement)
        {
            if (ModelState.IsValid)
            {
                db.CheckElements.Add(checkelement);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.CheckID = new SelectList(db.CheckDefs, "ID", "Name", checkelement.CheckID);
            ViewBag.UserID = new SelectList(db.Users, "ID", "Username", checkelement.UserID);
            return View(checkelement);
        }

        // GET: /HealthChecks/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CheckElement checkelement = db.CheckElements.Find(id);
            if (checkelement == null)
            {
                return HttpNotFound();
            }
            ViewBag.CheckID = new SelectList(db.CheckDefs, "ID", "Name", checkelement.CheckID);
            ViewBag.UserID = new SelectList(db.Users, "ID", "Username", checkelement.UserID);
            return View(checkelement);
        }

        // POST: /HealthChecks/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,CheckID,Status,Comment,UserID,Timestamp")] CheckElement checkelement)
        {
            if (ModelState.IsValid)
            {
                db.Entry(checkelement).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CheckID = new SelectList(db.CheckDefs, "ID", "Name", checkelement.CheckID);
            ViewBag.UserID = new SelectList(db.Users, "ID", "Username", checkelement.UserID);
            return View(checkelement);
        }

        // GET: /HealthChecks/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CheckElement checkelement = db.CheckElements.Find(id);
            if (checkelement == null)
            {
                return HttpNotFound();
            }
            return View(checkelement);
        }

        // POST: /HealthChecks/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            CheckElement checkelement = db.CheckElements.Find(id);
            db.CheckElements.Remove(checkelement);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
