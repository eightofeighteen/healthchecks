﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using HealthChecks.Models;

namespace HealthChecks.Controllers
{
    public class AdminController : Controller
    {
        private HealthChecksEntities db = new HealthChecksEntities();

        private int UserID(string username)
        {
            var userQuery =
                from a in db.Users
                where (a.Username.ToLower() == username.ToLower())
                select a;

            int userID = 0;
            foreach (var elem in userQuery)
            {
                userID = elem.ID;
            }
            return userID;
        }

        private int SiteID(string name)
        {
            var query =
                from a in db.Sites
                where (a.Name.ToLower() == name.ToLower())
                select a;
            foreach (var q in query)
                return q.ID;
            return 0;
        }

        private bool SiteExists(string name)
        {
            if (SiteID(name) == 0)
                return false;
            return true;
        }

        private void CreateSite(string sitename)
        {
            Site newsite = new Site();
            newsite.Name = sitename;
            newsite.Active = true;
            if (ModelState.IsValid)
            {
                db.Sites.Add(newsite);
                db.SaveChanges();
            }

            int siteID = SiteID(sitename);
            UserSite newmap = new UserSite();
            newmap.SiteID = siteID;
            newmap.UserID = UserID(User.Identity.Name);
            newmap.Flags = Flags.FLAG_SITEADMIN;
            if (ModelState.IsValid)
            {
                db.UserSites.Add(newmap);
                db.SaveChanges();
            }
        }

        public ActionResult NewSite(string sitename)
        {
            string error = "";
            if (String.IsNullOrEmpty(sitename) || String.IsNullOrWhiteSpace(sitename))
            {
                error = "Please enter the name of the new site.";
            }
            else
            {
                if (SiteExists(sitename))
                {
                    error = "The site \"" + sitename + "\" already exists.\nPlease enter another name or request access from the site admin.";
                }
                else
                {
                    CreateSite(sitename);
                    System.Web.Routing.RouteValueDictionary dict = new System.Web.Routing.RouteValueDictionary();
                    dict.Add("id", SiteID(sitename));
                    return RedirectToAction("RedirectToModifySite", dict);
                }
            }
            ViewBag.error = error;
            return View();
        }

        public ActionResult RedirectToModifySite(int id = 0)
        {
            ViewBag.ID = id;
            return View();
        }

        // GET: /Admin/
        public ActionResult Index()
        {
            return View(db.Sites.ToList());
        }

        // GET: /Admin/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Site site = db.Sites.Find(id);
            if (site == null)
            {
                return HttpNotFound();
            }
            return View(site);
        }

        // GET: /Admin/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: /Admin/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="ID,Name,Active")] Site site)
        {
            if (ModelState.IsValid)
            {
                db.Sites.Add(site);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(site);
        }

        // GET: /Admin/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Site site = db.Sites.Find(id);
            if (site == null)
            {
                return HttpNotFound();
            }
            return View(site);
        }

        // POST: /Admin/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="ID,Name,Active")] Site site)
        {
            if (ModelState.IsValid)
            {
                db.Entry(site).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(site);
        }

        // GET: /Admin/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Site site = db.Sites.Find(id);
            if (site == null)
            {
                return HttpNotFound();
            }
            return View(site);
        }

        // POST: /Admin/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Site site = db.Sites.Find(id);
            db.Sites.Remove(site);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
