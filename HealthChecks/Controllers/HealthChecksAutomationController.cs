﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HealthChecks.Models;

namespace HealthChecks.Controllers
{
    public class HealthChecksAutomationController : Controller
    {
        private IHealthChecksRepository _repository;

        public HealthChecksAutomationController() : this(new HealthChecksRepository())
        {

        }

        public HealthChecksAutomationController(IHealthChecksRepository repository)
        {
            _repository = repository;
        }

        //
        // GET: /HealthChecksAutomation/
        public ActionResult Index()
        {
            return View();
        }
	}
}