﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HealthChecks.Models;
using System.Net;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;

namespace HealthChecks.Controllers
{
    public class BackupAutomationController : Controller
    {
        private INVBackupRepository _repository;

        public BackupAutomationController() : this(new NVBackupRepository())
        {

        }

        public BackupAutomationController(INVBackupRepository repository)
        {
            _repository = repository;
        }

        


        //
        // GET: /BackupAutomation/
        public ActionResult Index()
        {
            NVBackupModel model = new NVBackupModel();
            model.AvailableHosts.Add(new SelectListItem { Text = "-Please select-", Value = "Selects hosts" });
            var hosts = _repository.GetAllHosts();
            foreach (var host in hosts)
            {
                model.AvailableHosts.Add(new SelectListItem()
                    {
                        Text = host.Client,
                        Value = host.Client
                    });
            }

            model.AvailableSites.Add(new SelectListItem { Text = "-Please select-", Value = "Selects sites" });
            var sites = _repository.GetAllSites();
            foreach (var site in sites)
            {
                model.AvailableSites.Add(new SelectListItem()
                    {
                        Text = site.Name,
                        Value = site.ID.ToString()
                    });
            }

            var autoTypes = _repository.GetAllAutoTypes();
            foreach (var at in autoTypes)
            {
                model.AvailableAutoTypes.Add(new SelectListItem()
                    {
                        Text = at.Name,
                        Value = at.ID.ToString()
                    });
            }

            return View(model);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult GetBackupJobsByHost(string host)
        {
            if (String.IsNullOrEmpty(host))
                throw new ArgumentNullException("host");
            var jobs = _repository.GetAllTitlesForHost(host);
            var j = Json(jobs, JsonRequestBehavior.AllowGet);
            return Json(jobs, JsonRequestBehavior.AllowGet);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult GetCheckDefsForSiteId(int siteId)
        {
            var checkdefs = _repository.GetAllCheckDefsBySiteID(siteId);
            var j = Json(checkdefs, JsonRequestBehavior.AllowGet);
            string s = j.ToString();
            var k = GetBackupJobsByHost("HOVH01");
            //return Json(checkdefs, JsonRequestBehavior.AllowGet);
            return j;
        }

        

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult CommitMap(string data)
        {
            if (String.IsNullOrEmpty(data))
                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError);
            ViewBag.data = data;
            JArray jsonData = JArray.Parse(data);
            List<IncomingJobCheckMap> incomingMap = new List<IncomingJobCheckMap>();
            foreach (var elem in jsonData)
            {
                IncomingJobCheckMap imap = new IncomingJobCheckMap(elem.ToString());
                incomingMap.Add(imap);
            }
            List<JobCheckMap> newList = new List<JobCheckMap>();
            foreach (var map in incomingMap)
            {
                string jobID = _repository.GetJobIDFromClientAndTitle(map.backupHost, map.backupJob);
                JobCheckMap newMap = new JobCheckMap();
                newMap.CheckDefID = map.checkID;
                newMap.JobID = jobID;
                newMap.JobClient = map.backupHost;
                newMap.AutoTypeID = map.type;
                newList.Add(newMap);
            }
            if (newList.Count > 0)
            {
                _repository.AddJobCheckMaps(newList);
            }

            return View();
        }
	}
}