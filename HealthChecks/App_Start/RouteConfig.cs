﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace HealthChecks
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "RouteMap2",
                url: "HealthChecks/ModifySite/{*pathInfo}");

            routes.MapRoute("GetBackupJobsByHost",
                            "BackupAutomation/GetBackupJobsByHost/{host}",
                            new { controller = "BackupAutomationController", action = "GetTitlesByHost" },
                            new[] { "HealthChecks.Controllers" });

            routes.MapRoute("GetCheckDefsForSiteId",
                            "BackupAutomation/GetCheckDefsForSiteId/",
                            new { controller = "BackupAutomationController", action = "GetCheckDefsForSiteId" },
                            new[] { "HealthChecks.Controllers" });
        }
    }
}
