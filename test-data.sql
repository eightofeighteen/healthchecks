﻿INSERT INTO
	dbo.Sites
	(Name, Active)
	VALUES
	('Head Office', 1),
	('Sezela', 1),
	('Pongola', 0);

INSERT INTO
	dbo.Users
	(Username, Active)
	VALUES
	('ZA\spurnell', 1),
	('ZA\dhardnick', 1),
	('ZA\bob.smith', 0);

INSERT INTO
	dbo.UserSite
	(SiteID, UserID)
	VALUES
	(1, 1),
	(2, 2),
	(3, 3),
	(2, 1);

INSERT INTO
	dbo.CheckDefs
	(SiteID, Name, Level, Parent, OrderNum)
	VALUES
	(1,'Physical Checks',0,0,100),
	(1,'Hardware Alerts',2,1,200),
	(1,'Aircons',2,1,300),
	(1,'Power & UPS',2,1,400),
	(1,'Backups',0,0,500),
	(1,'SnapManager for Hyper-V',1,5,600),
	(1,'hohvclus1_vmds_win1',2,6,700),
	(1,'hohvclus1_vmds_win2',2,6,800),
	(1,'hohvclus1_vmds_win3',2,6,900),
	(1,'hohvclus1_vmds_win4',2,6,1000),
	(1,'hohvclus1_vmds_linux1',2,6,1100),
	(1,'hohvclus1_vmds_linux2',2,6,1200),
	(1,'hohvclus1_vmds_linux3',2,6,1300),
	(1,'SnapRename',1,5,1400),
	(1,'hohvclus1_vmds_win1',2,14,1500),
	(1,'hohvclus1_vmds_win2',2,14,1600),
	(1,'hohvclus1_vmds_win3',2,14,1700),
	(1,'hohvclus1_vmds_win4',2,14,1800),
	(1,'hohvclus1_vmds_linux1',2,14,1900),
	(1,'hohvclus1_vmds_linux2',2,14,2000),
	(1,'hohvclus1_vmds_linux3',2,14,2100),
	(1,'Tapes',1,5,2200),
	(1,'hohvclus1_vmds_win1',2,22,2300),
	(1,'hohvclus1_vmds_win2',2,22,2400),
	(1,'hohvclus1_vmds_win3',2,22,2500),
	(1,'hohvclus1_vmds_win4',2,22,2600),
	(1,'hohvclus1_vmds_linux1',2,22,2700),
	(1,'hohvclus1_vmds_linux2',2,22,2800),
	(1,'hohvclus1_vmds_linux3',2,22,2900),
	(1,'Data',2,22,3000),
	(1,'Cifs',2,22,3100),
	(1,'HOTM1',2,22,3200),
	(1,'HOECM01',2,22,3300),
	(1,'Drive Snapshots',1,5,3400),
	(1,'HO-COGNOSSQL',2,34,3500),
	(1,'HO-COGNOSSQL01',2,34,3600),
	(1,'HO-COGNOSAPP',2,34,3700),
	(1,'HO-COGNOSAPP01',2,34,3800),
	(1,'HOTM1',2,34,3900),
	(1,'HOECM01',2,34,4000),
	(1,'SQL Servers',1,5,4100),
	(1,'HOSQL01',2,41,4200),
	(1,'HOSQL02',2,41,4300),
	(1,'HOSQL03',2,41,4400),
	(1,'HO-COGNOSSQL',2,41,4500),
	(1,'HO-COGNOSSQL01',2,41,4600),
	(1,'HOCM02',2,41,4700),
	(1,'HOCM03',2,41,4800);