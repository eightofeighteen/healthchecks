﻿--SELECT
--	s.Name, u.Username
--	FROM dbo.UserSite x
--	LEFT JOIN dbo.Users u ON
--		(x.UserID = u.ID)
--	LEFT JOIN dbo.Sites s ON
--		(x.SiteID = s.ID)
--	WHERE (s.ID = 1);

--DELETE
--	FROM dbo.CheckDefs
--	WHERE (ID >= 260);

--SELECT *
--	FROM dbo.CheckDefs
--	WHERE (SiteID = 1 AND ID >= 260);

--SELECT TOP 100 *
--	FROM dbo.CheckDefs
--	WHERE (Name = 'Hidden Check 1' OR ID >= 77);

--UPDATE dbo.CheckDefs
--	SET Active = 0
--	WHERE (Name = 'Hidden Check 1');

--INSERT INTO
--	dbo.CheckDefs
--	(SiteID, Name, Level, Parent, OrderNum, Active)
--	VALUES
--	(1, 'Hidden Check 1', 0, 0, 50, 1);

--ALTER TABLE
--	dbo.CheckDefs
--	ADD Active INT NOT NULL DEFAULT(1);

--ALTER TABLE
--	dbo.UserSite
--	ADD Flags INT NOT NULL DEFAULT(0);

--UPDATE
--	dbo.UserSite
--	SET Flags = 32768
--	WHERE (UserID = 1);

--DELETE
--	FROM dbo.UserSite
--	WHERE (ID >= 7);
--DELETE
--	FROM dbo.Sites
--	WHERE (ID >= 4);

--SELECT *
--	FROM dbo.CheckDefs
--	WHERE (SiteID >= 7);

--SELECT *
--	FROM dbo.UserSite
--	WHERE (UserID = 1);

--SELECT *
--	FROM dbo.Sites;

--CREATE VIEW
--	dbo.v_UsernamesForCheckElements
--AS
--SELECT DISTINCT u.Username
--	FROM dbo.CheckElements ce
--	INNER JOIN dbo.Users u ON
--	(u.ID = ce.UserID);

--SELECT COUNT(ID)
--	FROM dbo.CHeckElements
--	WHERE (Timestamp >= '2014/07/04');

--SELECT COUNT(ID)
--	FROM dbo.CheckDefs
--	WHERE (SiteID = 1 AND Active = 1 AND Level >= 2);

--DELETE FROM
--	dbo.Users
--	WHERE (ID = 6);
--DELETE FROM
--	dbo.UserSite
--	WHERE (ID >= 16);

--SELECT *
--	FROM dbo.Users;

--SELECT us.ID, us.SiteID, us.UserID, us.Flags, u.username
--	FROM dbo.UserSite us
--	LEFT JOIN dbo.Users u ON
--	(u.ID = us.UserID)
--	where (SiteID = 1);

--SELECT u.Username, s.Name
--	FROM dbo.UserSite us
--	LEFT JOIN dbo.Sites s ON
--	(us.SiteID = s.ID)
--	LEFT JOIN dbo.Users u ON
--	(us.UserID = u.ID)
--	WHERE (u.Username = 'ZA\spurnell');

--UPDATE 
--	dbo.Users
--	SET Username = 'ZA\spurnell2'
--	WHERE (ID = 1);

--INSERT
--	INTO dbo.UserGroup
--	(UserID, GroupID)
--	VALUES
--	(7, 1);

--INSERT
--	INTO dbo.GroupSite
--	(SiteID, GroupID, Flags)
--	VALUES
--	(2, 1, 31768);
--UPDATE
--	dbo.GroupSite
--	SET Flags = 32768
--	WHERE ID = 2;

--SELECT *
--	FROM dbo.GroupSite;

--SELECT *
--	FROM dbo.Sites;

--SELECT gs.ID, s.Name, g.Name
--	FROM dbo.GroupSite gs
--	LEFT JOIN dbo.Sites s ON
--		s.ID = gs.SiteID
--	LEFT JOIN dbo.Groups g ON
--		g.ID = gs.GroupID;

--SELECT ug.ID, u.Username, g.Name
--	FROM dbo.UserGroup ug
--	LEFT JOIN dbo.Users u ON
--		u.ID = ug.UserID
--	LEFT JOIN dbo.Groups g ON
--		g.ID = ug.GroupID;

--SELECT *
--	FROM dbo.LateReasons;

SELECT *
	FROM dbo.Users
	WHERE (Username = 'BatchProcessor');